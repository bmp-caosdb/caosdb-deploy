# Start CaosDB #

Want to try it out?  Try these steps:

1. `./caostart`
   1. If you get `docker: command not found`, make sure that docker and
      docker-compose are installed and that you have permissions to run it.
   2. On Debian/Ubuntu, you can install it with `apt-get install docker.io
      docker-compose`.  Alternatively, install docker-compose locally (and in a
      newer version) with `pip3 install --user docker-compose`
   3. It might be necessary on your system to start and / or enable the docker service manually. On systems with systemd this is usually done using `sudo systemctl start docker` and `sudo systemctl enable docker`.
   4. Add yourself to the `docker` group: `usermod -aG docker USERNAME` and
      login again.
2. In your preferred browser, open `https://localhost:10443/`, ignore TLS
   warnings. Login with `admin`, password `caosdb`.
   - Reload the browser page if no **Welcome to CaosDB!** page appears.
3. Shutdown the server with `Ctrl-c`, followed by `docker-compose --file
   compose/docker-compose.yml down`.

# Profiles #

The caosdb docker image can be customizes with profiles. Please have
a look at the docstring of the `caostart` for further information about profiles
and directory structures or take a look at the default profile in [profiles/default/](profiles/default/).

