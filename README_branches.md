# Branches of this repository

Branches in this repository are a bit special, because some of them have
hard-coded git references.  This is the used nomenclature:

- `master` :: The `master` branch is where development of the deploy
  repository takes place.
- Branches named after caosdb's branches :: If a branch is named like a branch
  of other caosdb repositories, it should use references that are known to
  compile or even better work.  Notable examples are:
  - dev
  - f-server-side-scripting
- `deploy-*` :: These are feature branches for development of the deploy
  repository.
