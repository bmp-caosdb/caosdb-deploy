#!/bin/bash
# ** header v3.0
# This file is a part of the CaosDB Project.

# Copyright (C) 2019 Daniel Hornung <d.hornung@indiscale.com>

# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as
# published by the Free Software Foundation, either version 3 of the
# License, or (at your option) any later version.

# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.

# You should have received a copy of the GNU Affero General Public License
# along with this program. If not, see <https://www.gnu.org/licenses/>.

# ** end header

# Run backup inside the docker container and download dump
# Very useful for e.g. automatic backup scripts and / or cronjobs
# A. Schlemmer, 05/2019

# Trying to parse the location of the dumped file.
# No error handling yet.
dump_location=$(docker exec -w /opt/caosdb/git/caosdb-mysqlbackend/ \
                       compose_caosdb-server_1 ./backup.sh  --backupdir=/tmp \
                    | sed -n '/^Dumping database .* to /p' \
                    | grep -o '/tmp/caosdb\..*\.sql'\
             )
dump_filename="${dump_location##/tmp/}"

echo "Trying to copy SQL dump $dump_filename here."
docker cp compose_caosdb-server_1:"$dump_location" . || {
    echo "Error while copying SQL dump."
    exit 1
}

echo "Successfully copied SQL dump."
echo "SQL file name: $dump_filename"
