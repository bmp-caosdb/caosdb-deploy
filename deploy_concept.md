# Concept #
1. Docker images for
   - MariaDB (from upstream)
     - Has the root password set.
     - Does not yet create a caosdb user and database.
       - This might change in the future, but keeps compatibility with the
         current SQL setup process.
   - CaosDB server (initially always with web ui content)
2. Docker-compose file with basic configuration of the images.
   1. SQL server starts as plain container.
   2. CaosDB starts with wrapper script which does in a loop:
      1. Wait for the SQL server to be available
      2. Check if SQL server is configured correctly, configures it if not.
      3. Start CaosDB
3. Script to automatically fill the Docker compose file with configuration and
   to create the containers from it.
4. Startup service for systemd.
5. All of the above packaged into a .deb package.
