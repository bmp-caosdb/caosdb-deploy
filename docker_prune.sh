#!/usr/bin/env bash

# Stops all running CaosDB docker containers and tries to remove the docker
# images as well.

set -euo pipefail
IFS=$'\n\t'

read -p "Do you really want to stop and delete all CaosDB containers and images?
You will need to rebuild the image afterwards. [yN] " really

if [[ "${#really}" == 0 || "${really,,}" == "n" ]]; then
    exit 0
fi

echo "Stopping running containers..."
running_caosdb=$(docker ps -f "name=caosdb:*" -q)
for running in $running_caosdb; do
    docker stop "$running"
done

echo "Pruning stopped containers..."
docker container prune -f
# containers_caosdb=$(docker ps -a -f "name=caosdb:*" -q)
# for cont in $containers_caosdb; do
#     docker container rm -v "$cont"
# done

echo "Removing images..."
images_caosdb=$(docker image ls -q caosdb)
for img in $images_caosdb; do
    docker image rm "$img"
done
docker image prune -f
