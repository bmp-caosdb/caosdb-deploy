#!/usr/bin/env python3
# encoding: utf-8
""" Use case analysis. 

Example how to download files analyse them and push the results back into caosdb.
"""

from urllib.request import urlopen
import caosdb as db
from data_model_definition import get_entity

db.Container._debug=lambda:0
db.get_connection()._debug=lambda:0

configuration = {}
configuration["resolution"] = 0.01
configuration["responsible"] = "Anna Lytik"
configuration["inputExperiment"] = db.execute_query("FIND RECORD UnicornExperiment WITH date in 2025-10", unique=True).id
configuration["inputSimulation"] = db.execute_query("FIND RECORD MySimulation WITH Width = 200px AND Height = 225px", unique=True).id


# query files and download
ecg_entity = db.execute_query("FIND FILE UnicornECG WHICH IS REFERENCED BY " + str(configuration["inputExperiment"]), unique=True).download('./ecg.data')

simulation_results_entity = db.execute_query("FIND FILE Results WHICH IS REFERENCED BY " + str(configuration["inputSimulation"]), unique=True).download('./sim_results.tsv')



# do analysis
print("read ecg data")
with open('./ecg.data','r') as local_ecg_file:
    print(local_ecg_file.read())

print("read simulation results")
with open('./sim_results.tsv', 'r') as local_simulation_results_file:
    print(local_simulation_results_file.read())

print("start algorithm with --resolution=0.01")
plot = urlopen("http://matplotlib.org/mpl_examples/axes_grid/scatter_hist.png")
with open('plot.png', 'wb') as output:
    while True:
        data = plot.read(4096)
        if data:
            output.write(data)
        else:
            break
    print("[OK]")
    print("create plot")

with open('results.npy','w') as output:
    print("store results to results.npy")
    output.write("This is results.npy")

import pickle
with open("configuration.pickle", "wb") as output:
    print("write configuration to pickle")
    pickle.dump(configuration, output)

print("FINISHED WITHOUT ERRORS")
