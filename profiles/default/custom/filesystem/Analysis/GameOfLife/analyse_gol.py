#!/usr/bin/env python3
# Script to analyse npy file and generate a video
# A. Schlemmer, 12/2018

from matplotlib import pyplot as plt
import numpy as np
import matplotlib.animation as anim

(fig, ax) = plt.subplots(1, 2, figsize=(12,4))

dat = np.load("gol.npy")

ax = plt.subplot2grid((1, 3), (0, 0), 1, 1)
im = ax.imshow(dat[25,...], cmap="magma_r", origin="lower")
ax.set_xlabel("X")
ax.set_ylabel("Y")

ax = plt.subplot2grid((1, 3), (0, 1), 1, 2)
ax.plot(np.arange(dat.shape[0]), dat.sum(axis=(1,2)))

ax.set_ylabel("Number of activated pixels")
ax.set_xlabel("Time")
line = plt.axvline(0, color="green")
plt.tight_layout()

sampst = 1
T = dat.shape[0] // sampst

def upd(i, im, data):
    im.set_data(data[i*sampst,...])
    line.set_xdata((i*sampst,))
    return (im,line)

an = anim.FuncAnimation(fig, upd, T,
                        fargs=(im, dat,),
                        interval=50,
                        blit=True)
an.save("data_gol.webm", bitrate=2000, codec="libvpx-vp9")
