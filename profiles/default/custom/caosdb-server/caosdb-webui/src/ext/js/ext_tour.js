$(document).ready(() => {
    if ($("div.caosdb-f-main-entities").length == 0 && $('.caosdb-user-name').length > 0) {
        console.log("empty");
        location.href = "/Entity/99";
    };


    if (document.getElementsByClassName("caosdb-user-name").length == 0 || document.getElementsByClassName("caosdb-user-name")[0].innerText === "guest") {
        $("button.caosdb-new-comment-button").hide();
        if ($('.caosdb-query-response-results').length == 1 && $('.caosdb-query-response-results')[0].innerText !== "1") {
            $(".caosdb-entity-panel").each(function(index) {
                var parentName = $(this).find('.caosdb-parent-name');
                if (parentName.length > 0 && parentName[0].innerText === "CommentAnnotation") {
                    $(this).hide();
                };
            });
        };
        // remove debug output from query
    };
    if ($('.caosdb-id').length == 1 && $('.caosdb-id')[0].innerText === "99") {
        $('#99 > .caosdb-entity-panel-heading').remove();
    };
    var tourguide = $('<li><a href="/Entity/99">Start a Tour</a></li>');
    $('ul.caosdb-navbar').append(tourguide);

});
