#!/bin/bash

# ** header v3.0
# This file is a part of the CaosDB Project.
#
# Copyright (C) 2019 Daniel Hornung, Göttingen
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as
# published by the Free Software Foundation, either version 3 of the
# License, or (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program. If not, see <https://www.gnu.org/licenses/>.
#
# ** end header

# To fail fast, but beware https://mywiki.wooledge.org/BashFAQ/105
set -e

############# User configurables #################
# Remotes
REM_MYSQLBACKEND="https://gitlab.gwdg.de/bmp-caosdb/caosdb-mysqlbackend.git"
REM_SERVER="https://gitlab.gwdg.de/bmp-caosdb/caosdb-server.git"
REM_PYLIB="https://gitlab.gwdg.de/bmp-caosdb/caosdb-pylib.git"


# Exact references
REF_MYSQLBACKEND="ddc53f18"
REF_SERVER="aa2f93ae"
# Set to an empty string if the WebUI should be at the version set in the parent
REF_WEBUI=""
REF_PYLIB="3eee57b"

if [ -f /opt/caosdb/build_docker/refs.conf ]; then
   . /opt/caosdb/build_docker/refs.conf
fi

############# End of user configurables #################

function fail() {
    echo "Some error occured, exiting."
    exit 1
}

function clone_caosdb() {
    mkdir -p git
    cd git
    git clone "$REM_PYLIB" || true
    pushd caosdb-pylib
    git checkout "$REF_PYLIB"
    popd
    git clone "$REM_SERVER" || true
    pushd caosdb-server
    # make easy-units
    # mvn dependency:copy-dependencies
    git checkout "$REF_SERVER"
    git submodule update --init caosdb-webui
    if [[ -n "$REF_WEBUI" ]] ; then
        pushd caosdb-webui
        git checkout "$REF_WEBUI"
        popd
    fi
}

function make_caosdb() {
    # python client
    pushd git/caosdb-pylib
    echo "Installing pylib"
    python3 setup.py install
    # TODO this is a dirty work-around
    ln -s /opt/caosdb/mnt/other/.pycaosdb.ini /.pycaosdb.ini
    popd
    # CaosDB itself
    pushd git/caosdb-server
    pushd caosdb-webui
    echo "Making webui"
    make
    popd
    pushd misc/pam_authentication/
    make
    popd
    # make compile
    echo "Making CaosDB server jar"
    make jar
    popd
}

function make_run() {
    # Startup helper tools
    pushd run_docker/utils
    tools="
        ypsetup
        tzsetup
        mail_setup
        permission_setup
    "
    for tool in $tools; do
        g++ -std=c++17 -o "$tool" src/"$tool".cpp \
            -lboost_system -lboost_filesystem -ldl -lstdc++fs
        chown root "$tool"
        chmod u+s "$tool"
    done
    popd

    # Modify pam files
    sed -i -e 's/\(compat\)/\1 nis/' /etc/nsswitch.conf
}

function config_caosdb() {
    cd git/caosdb-server/conf/core
    ../../../../build_docker/create_server_conf.py . > ../ext/server.conf
    mkdir -p /tmp/caosdb/tmpfiles
    mkdir -p /opt/caosdb/mnt/caosdb-server
    mkdir -p /opt/caosdb/mnt/other
}

function prepare_sql() {
    mkdir -p git
    pushd git
    git clone "$REM_MYSQLBACKEND"
    cd caosdb-mysqlbackend
    git checkout "$REF_MYSQLBACKEND"
    popd
    ls -l
    ls -l git
    cp build_docker/sql.config git/caosdb-mysqlbackend/.config
}

function cert() {
    mkdir -p cert
    cd cert
    KEYPW="CaosDBSecret"
    KEYSTOREPW="CaosDBKeyStore"
    KEYSTOREPW="$KEYPW"
    # NOTE: KEYPW and KEYSTOREPW are the same, due to Java limitations.
    KEYPW="$KEYPW" openssl genrsa -aes256 -out caosdb.key.pem \
         -passout env:KEYPW 2048
    # Certificate is for localhost
    KEYPW="$KEYPW" openssl req -new -x509 -key caosdb.key.pem \
         -out caosdb.cert.pem -passin env:KEYPW \
         -subj "/C=/ST=/L=/O=/OU=/CN=localhost"
    KEYPW="$KEYPW" KEYSTOREPW="$KEYSTOREPW" openssl pkcs12 -export \
         -inkey caosdb.key.pem -in caosdb.cert.pem -out all-certs.pkcs12 \
         -passin env:KEYPW -passout env:KEYPW

    keytool -importkeystore -srckeystore all-certs.pkcs12 -srcstoretype PKCS12 \
            -deststoretype pkcs12 -destkeystore caosdb.jks \
            -srcstorepass "$KEYPW" \
            -destkeypass "$KEYPW" -deststorepass "$KEYSTOREPW"
    echo "Certificates successfuly created."
}

# Adds an admin user, including PAM setup in the usersources.ini
function user() {
    PASSWORD="caosdb"
    useradd admin
    echo -e "${PASSWORD}\n${PASSWORD}" | passwd admin
    echo "Adding CaosDB user 'admin' with password 'caosdb'."
    INI_TXT=$(cat <<EOF
realms = PAM
defaultRealm = PAM
[PAM]
class = caosdb.server.accessControl.Pam
default_status = ACTIVE
include.user = admin
user.admin.roles = administration
EOF
           )
    echo "$INI_TXT" > git/caosdb-server/conf/ext/usersources.ini
}

function drop_privilege() {
    chmod -R a+rwX cert
    cd git
    chmod -R a+rwX caosdb-server caosdb-mysqlbackend
    chmod -R a+rwX /tmp/caosdb/tmpfiles
    chmod -R a+rwX /opt/caosdb/m2
    # We need this for the "PAM inside Docker" authentication
    chown :shadow \
          caosdb-server/misc/pam_authentication/bin/pam_authentication
    chmod g+s caosdb-server/misc/pam_authentication/bin/pam_authentication
}

case $1 in
    "os") echo "Not supported anymore, call setup_os.sh instead."; exit 1 ;;
    "clone") clone_caosdb ;;
    "config") config_caosdb ;;
    "make") make_caosdb ;;
    "make_run") make_run ;;
    "cert") cert ;;
    "prep_sql") prepare_sql ;;
    "user") user ;;
    "privilege") drop_privilege ;;
    *) echo "Unknown action: $1"; exit 1
esac
