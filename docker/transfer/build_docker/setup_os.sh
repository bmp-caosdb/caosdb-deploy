#!/bin/bash

# ** header v3.0
# This file is a part of the CaosDB Project.
#
# Copyright (C) 2019 Daniel Hornung, Göttingen
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as
# published by the Free Software Foundation, either version 3 of the
# License, or (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program. If not, see <https://www.gnu.org/licenses/>.
#
# ** end header

# To fail fast, but beware https://mywiki.wooledge.org/BashFAQ/105
set -e

function setup_os() {
    # - mariadb-client :: For SQL server configuration.
    PACKAGES="
    build-essential
    git
    libboost-dev
    libboost-filesystem-dev
    libboost-system-dev
    libpam0g-dev
    make
    mariadb-client
    maven
    nis
    openjdk-8-jdk-headless
    postfix
    python3-pip
    screen
    unzip
    libxml2-dev
    libxslt1-dev
    zlib1g
    zlib1g-dev
    python3-numpy
    python3-pandas
    python3-xlrd
    libyaml-cpp-dev
    "
    # vim etc :: For debugging / development
    PACKAGES+="vim
    emacs-nox
    htop
    procps
    bash-completion
    "
    export DEBIAN_FRONTEND="noninteractive"
    apt-get update
    apt-get dist-upgrade -y
    apt-get install -y $PACKAGES

    # For debugging / development
    echo ". /etc/bash_completion" >> /etc/bash.bashrc
}

# Just call the function
setup_os
