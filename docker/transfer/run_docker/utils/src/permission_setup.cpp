// ** header v3.0
// This file is a part of the CaosDB Project.
//
// Copyright (C) 2019 Daniel Hornung <d.hornung@indiscale.com>
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU Affero General Public License as
// published by the Free Software Foundation, either version 3 of the
// License, or (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU Affero General Public License for more details.
//
// You should have received a copy of the GNU Affero General Public License
// along with this program. If not, see <https://www.gnu.org/licenses/>.
//
// ** end header

/**
 * This program fixes incorrect permissions.
 */

#include <fstream>
#include <iostream>
#include <unistd.h>
#include <pwd.h>
#include <unistd.h>
#include <sys/stat.h>

// Compile it with (the order of the cpp and the libs matters):
// g++ -o permission_setup src/permission_setup.cpp

void fix_permission(const std::string& dir, unsigned long uid, mode_t perms);

/**
 * Positional arguments:
 * - uid : The UID to set the directories to.
 */
int main(int argc, char* argv[]) {

  if (argc != 2) {
    std::cerr << "Target UID required as the argument!" << std::endl;
    std::cerr << "You gave " << argc - 1 << " arguments instead." << std::endl;
    return 1;
  }
  unsigned long uid = std::stoul(argv[1]);

  fix_permission("/opt/caosdb/filesystem", uid, 0755) ;
  fix_permission("/opt/caosdb/dropoffbox", uid, 0733);

  return 0;
}

/**
 * Arguments:
 * ==========
 * - dir
 * - uid
 * - user perms
 */
void fix_permission(const std::string& dir, unsigned long uid, mode_t perms) {
  // all in plain C, unfortunately
  chown(dir.c_str(), (uid_t) uid, -1);
  chmod(dir.c_str(), perms);
}
